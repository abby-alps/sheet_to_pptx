import click, openpyxl
from pptx import Presentation

@click.command()
@click.argument('workbookpath', type=click.Path(exists=True))
@click.argument('sheetname')
@click.argument('outppt', type=click.Path(exists=True))
def open_book(workbookpath, sheetname, outppt):
	# brief desc and help messages for command line args
	"""Uses specified WORKBOOKPATH's SHEETNAME to turn data from cells into the given OUTPPT.

	WORKBOOKPATH is the path to the workbook\n
	SHEETNAME is the name of the sheet you wish to copy data from\n
	OUTPPT is the powerpoint to edit with data from excel\n
	"""
	click.echo("opening book...")
	wb = openpyxl.load_workbook(click.format_filename(workbookpath)) # opens workbook
	click.echo("getting sheet...")
	sheet = wb[sheetname] # accesses specified worksheet
	click.echo("prepping and outputting data")
	out_prs = Presentation(outppt) # open specified powerpoint file
	excel_txt = get_sheet_info(sheet, out_prs)
	text_to_pres(outppt, sheet, out_prs, excel_txt)
	excel_txt.close()
	out_prs.save(outppt)
	print("finished")

# reads necessary cells' data, writes + formats to a text file
def get_sheet_info(sheet, out_prs):
	cell_data = open("textOut.txt", "w+")
	row_string = ""
	for row in range(3, sheet.max_row + 1):
		row_string += str(sheet['A' + str(row)].value)
		row_string += " "
		if str(sheet['B' + str(row)].value) == "Requirement":
			row_string += str(sheet['B' + str(row)].value)
		else:
			row_string += str(sheet['C' + str(row)].value)
		row_string += ": "
		if sheet['D' + str(row)].value == None:
			row_string += str(sheet['E' + str(row)].value)
		else:
			row_string += str(sheet['D' + str(row)].value)
		cell_data.writelines(row_string + "\n")

		row_string = ""
	print(cell_data.readline())

	return cell_data

# reads text file to organize and write lines to pptx
def text_to_pres(outppt, sheet, out_prs, excel_txt):
	print("creating presentation...")

	num_source = 0
	num_der = 0
	slide = out_prs.slides.add_slide(out_prs.slide_layouts[1])
	shapes = slide.shapes
	
	title_shape = shapes.title
	body_shape = shapes.placeholders[10]

	title_shape.text = 'IPS Troubleshooting Requirements - ' + str(sheet.title)

	tf = body_shape.text_frame

	excel_txt.seek(0, 0)
	
	for line in excel_txt:
		if "Requirement" not in line and num_source < 3: # add source requirement to current slide
			#tf.text = 'curr Source: '
			p = tf.add_paragraph()
			p.text = 'Source Requirement: '
			p.level = 0
			p = tf.add_paragraph()
			p.text = line.strip('\n')
			p.level = 1
			num_source += 1
		elif num_source >= 3: # create new slide to add a source requirement to
			slide = out_prs.slides.add_slide(out_prs.slide_layouts[1])
			shapes = slide.shapes
	
			title_shape = shapes.title
			body_shape = shapes.placeholders[10]

			title_shape.text = 'IPS Troubleshooting Requirements - ' + str(sheet.title)
			tf = body_shape.text_frame

			p = tf.add_paragraph()
			p.text = 'Source Requirement: '
			p.level = 0
			p = tf.add_paragraph()
			p.text = line.strip('\n')
			p.level = 1
			num_source = 1
			num_der = 0
		elif "Requirement" in line and num_der < 6: # add derived requirement to current slide
			# derived req, current slide
			p = tf.add_paragraph()
			p.text = 'Derived Requirement: '
			p.level = 2
			p = tf.add_paragraph() #derived requirement format
			p.text = line.strip('\n')
			p.level = 3
			num_der += 1
		else: # create a new slide to add derived requirement(s) (if current slide is too full)
			print(num_der)
			if("Requirement" in line):
				slide = out_prs.slides.add_slide(out_prs.slide_layouts[1])
				shapes = slide.shapes

				title_shape = shapes.title
				body_shape = shapes.placeholders[10]

				title_shape.text = 'IPS Troubleshooting Requirements - ' + str(sheet.title)
				tf = body_shape.text_frame

				p = tf.add_paragraph()
				p.text = 'Derived Requirement NEW: '
				p.level = 2
				p = tf.add_paragraph()
				p.text = line.strip('\n')
				p.level = 3

				num_der = 0

if __name__ == '__main__':
	open_book()